class Product < ActiveRecord::Base
  mount_uploader :image, ImageUploader
  process_in_background :image
end
